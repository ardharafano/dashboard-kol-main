import { resolve } from "path";
import { defineConfig } from "vite";

export default defineConfig({
    build: {
        rollupOptions: {
            input: {
                main: resolve(__dirname, "index.html"),
                "influencer-finder": resolve(
                    __dirname,
                    "influencer-finder.html"
                ),
                "influencer-profile": resolve(
                    __dirname,
                    "influencer-profile.html"
                ),
            },
        },
    },
});
