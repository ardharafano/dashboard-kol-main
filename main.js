import "./style.css";
document.addEventListener("DOMContentLoaded", function () {
  setTimeout(() => {
    window.scrollTo(0, 0);
  }, 2000);
  document.querySelector(".btn-navbar").addEventListener("click", function () {
    document
      .querySelector(".navbar-menu")
      .classList.toggle("-translate-y-full");
  });
});
